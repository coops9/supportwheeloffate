﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class StaffRepository : IStaffRepository, IDisposable
    {
        private WheelOfFateEntities context;

        public StaffRepository()
        {
            context = new WheelOfFateEntities();
        }

        /*Disposing of the DBContext is not strictly necessary thanks to EntityFramework handling the connection https://blog.jongallant.com/2012/10/do-i-have-to-call-dispose-on-dbcontext/
         *but lets show usage of the disposing pattern and understanding of it's existance. No harm in having this*/
        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public IEnumerable<Staff> GetActiveStaff()
        {
            var context = new WheelOfFateEntities();
            return context.Staffs.Where(x => x.IsActive).ToList();
        }
    }
}
