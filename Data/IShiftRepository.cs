﻿using System.Collections.Generic;

namespace Data
{
    public interface IShiftRepository
    {
        void InsertRota(List<Shift> shifts);
        List<Shift> GetRota();
    }
}