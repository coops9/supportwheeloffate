﻿using System.Collections.Generic;

namespace Data
{
    public interface IStaffRepository
    {
        IEnumerable<Staff> GetActiveStaff();
    }
}