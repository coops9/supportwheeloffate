﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class ShiftRepository : IShiftRepository, IDisposable
    {
        private WheelOfFateEntities context;

        public ShiftRepository()
        {
            context = new WheelOfFateEntities();
        }

        /// <summary>
        /// Clear any old Rota
        /// </summary>
        /// <remarks>Completely clear old Rota data from the database</remarks>
        public void ClearRota()
        {
            /*Completely clear the SQL table and reset the primary key - start again effectively
             * You don't normally use such a command in a production environment
             * todo: Change to DELETE command or change to deleting specific records by id*/
             /*This is not ideal as it's not using LINQ to SQL but it's more efficient than
              * the EF generated script which will delete row-by-row*/
            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Shift]");
        }

        public List<Shift> GetRota()
        {
            return context.Shifts.ToList();
        }

        public void InsertRota(List<Shift> shifts)
        {
            context.Shifts.AddRange(shifts);
            context.SaveChanges();
        }

        /*Disposing of the DBContext is not strictly necessary thanks to EntityFramework handling the connection https://blog.jongallant.com/2012/10/do-i-have-to-call-dispose-on-dbcontext/
         *but lets show usage of the disposing pattern and understanding of it's existance. No harm in having this*/
        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
    }
}
