﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WheelOfFateRota;
using WheelOfFateRota.Controllers;
using WheelOfFateRota.Business;

namespace WheelOfFateRota.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        private const int SampleTotalWorkingDays = 30;
        private const int TwoWeekWorkingDays = 10;
        private const int NumberOfTimesToTestRotaGeneration = 2;

        /// <summary>
        /// Test all engineers have 2 shifts in a two week period. Get a 2 week shift a number of times e.g. 20
        /// </summary>
        [TestMethod]
        public void TestAllEngineersHaveRightNumberOfShiftsInATwoWeekPeriod()
        {
            //The staff assignment is random, so we want to test a number of two week rotas
            for (int i = 0; i < NumberOfTimesToTestRotaGeneration; i++)
            {
                TestAllEngineersHaveRightNumberOfShifts();
            }
        }

        private static void TestAllEngineersHaveRightNumberOfShifts()
        {
            var shiftView = new ShiftView() { DatabasePersistence = false};
            //Number of staff equats to the same amount of minimum days for everyone to have a shift
            int shiftLengthDaysForEveryoneToHaveAShift = shiftView.GetStaffShiftList().Count();

            var rota = shiftView.GetRota(shiftLengthDaysForEveryoneToHaveAShift);

            //Get all instances of staff from all shifts
            var shiftStaff = new List<int>();
            foreach (var shift in rota)
            {
                shiftStaff.Add(shift.ShiftStaffMemberAM.Id);
                shiftStaff.Add(shift.ShiftStaffMemberPM.Id);
            }

            //Check that there everyone was assigned 2 shift, no more, no less
            Assert.IsFalse(shiftStaff.GroupBy(x => x).Any(x => x.Count() != 2));
        }

        /// <summary>
        /// Staff cannot do shifts on consecutive day. Test generation of a large number of days e.g. 5000
        /// </summary>
        [TestMethod]
        public void TestNonConsecutiveDays()
        {
            var shiftView = new ShiftView() { DatabasePersistence = false };
            var rota = shiftView.GetRota(SampleTotalWorkingDays);

            ShiftView lastShift = null;
            foreach (var currentShift in rota)
            {
                if(lastShift == null)
                {
                    lastShift = currentShift;
                    continue;
                }

                //Compare current shift to last shift
                //todo: This is not very DRY
                int currentShiftStaffMemberAM = currentShift.ShiftStaffMemberAM.Id;
                Assert.IsFalse(currentShiftStaffMemberAM == lastShift.ShiftStaffMemberAM.Id ||
                                currentShiftStaffMemberAM == lastShift.ShiftStaffMemberPM.Id, 
                                $"Engineer: {currentShiftStaffMemberAM} has been assigned to consecutive shifts {currentShift.Date}");
                int currentShiftStaffMemberPM = currentShift.ShiftStaffMemberPM.Id;
                Assert.IsFalse(currentShiftStaffMemberPM == lastShift.ShiftStaffMemberAM.Id ||
                                currentShiftStaffMemberPM == lastShift.ShiftStaffMemberPM.Id, 
                                $"Engineer: {currentShiftStaffMemberPM} has been assigned to consecutive shifts {currentShift.Date}");

                lastShift = currentShift;
            }
        }
    }
}
