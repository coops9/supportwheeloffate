The code needs an MS SQL database to work against but the code can be downloaded and reviewed in VS

Typically you would exclude passwords from Git commits. In this case as it's just a test project so I have left them in but this is not good practice normally.