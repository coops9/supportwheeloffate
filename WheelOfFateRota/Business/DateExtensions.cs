﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WheelOfFateRota.Business
{
    public static class DateExtensions
    {
        public static bool IsWeekendDate(this DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }

        public static bool IsHoliday(this DateTime date)
        {
            var holidays = new List<DateTime>();
            holidays.Add(new DateTime(2018, 05, 14));
            holidays.Add(new DateTime(2018, 05, 18));
            holidays.Add(new DateTime(2018, 05, 21));
            holidays.Add(new DateTime(2018, 05, 25));

            return holidays.Contains(date.Date);
        }

        public static DateTime GetNextWorkingDate(this DateTime date)
        {
            //do-while so the action occurs before the conditional check
            do
            {
                date = date.AddDays(1);
            }
            while (date.IsWeekendDate() || date.IsHoliday());

            return date;
        }

        public static DateTime GetPreviousWorkingDay(this DateTime date)
        {
            //do-while so the action occurs before the conditional check
            do
            {
                date = date.AddDays(-1);
            }
            while (date.IsWeekendDate() || date.IsHoliday());

            return date;
        }
    }
}