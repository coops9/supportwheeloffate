﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace WheelOfFateRota.Business
{
    //Todo: Multiple uses of dictionaries can be a bit unweildy... maybe don't use them as much
    public class ShiftView
    {
        public DateTime Date { get; set; }
        public StaffView ShiftStaffMemberAM { get; set; }
        public StaffView ShiftStaffMemberPM { get; set; }
        public bool RotaReset { get; set; }
        public bool DatabasePersistence { get; set; }

        private Dictionary<int, string> Staff { get; set; }

        public ShiftView()
        {
            DatabasePersistence = true;

            using (var repository = new StaffRepository())
            {
                //Lets store a copy of the staff list 
                Staff = repository.GetActiveStaff().ToDictionary(x => x.Id, x => x.Name);
            }
        }

        /// <summary>
        /// Get the list of shifts for the rota
        /// </summary>
        /// <param name="totalWorkingDays">The tota amount of working days to generate a rota for</param>
        /// <returns>A rota</returns>
        /// <remarks>Also saves the rota in the Database at the same time</remarks>
        public IEnumerable<ShiftView> GetRota(bool newRota = false, int totalWorkingDays = 20)
        {
            //Get the Rota
            List<ShiftView> rota = new List<ShiftView>();
            using (var repository = new ShiftRepository())
            {
                if (newRota)
                {
                    repository.ClearRota();
                }
                else
                {
                    //Gt existing Rota from the DB and map it to the view object type
                    rota = repository.GetRota().Select(x => new ShiftView()
                    {
                        Date = x.Date,
                        ShiftStaffMemberAM = new StaffView() { Name = Staff[x.ShiftAMStaffId] },
                        ShiftStaffMemberPM = new StaffView() { Name = Staff[x.ShiftPMStaffId] }
                    }).ToList();
                }
            }

            //If there is no existing rota in the database then we generate one (same applies for new rota)
            if (!rota.Any())
            {
                rota = GenerateRota(totalWorkingDays);

                //Save the new Rota in the Database for persistance
                if (DatabasePersistence)
                {
                    /*This context will create an implicit SQL Transaction, executed on the call of save,
                     * this is great as we don't have manage the transation and we get an 
                     * Atomic all or nothing commit to the database*/
                    using (var repository = new ShiftRepository())
                    {
                        repository.InsertRota(rota.Select(x =>
                            new Shift()
                            {
                                Date = x.Date,
                                ShiftAMStaffId = x.ShiftStaffMemberAM.Id,
                                ShiftPMStaffId = x.ShiftStaffMemberPM.Id
                            }).ToList());
                    }
                }
            }

            return rota;
        }

        /// <summary>
        /// Generate a rota by randomly assigning staff to shifts
        /// </summary>
        /// <param name="totalWorkingDays">The tota amount of working days to generate a rota for</param>
        /// <returns>A rota</returns>
        private List<ShiftView> GenerateRota(int totalWorkingDays)
        {
            var rota = new List<ShiftView>();

            //Staff Id, Tracking the last assigned shift date (Could use a new business class instead, but the dictionary has a fast lookup)
            Dictionary<int, DateTime?> staffPool = GetStaffShiftList();

            /* Create an instance of Random and then reuse it so we get uniformity in the random numbers, using the same seed
             * Sufficiently random for this project I think, althoguh NOT if multiple lists are generated
               in quick succession as the seed will be the same or very similar*/
            Random random = new Random();

            var startDate = DateTime.Now.Date;

            //Considered using a nullable integer but don't like overusing nullable objects as null ref is more likely overall if you do
            int rotaChangeLastAssignedStaffOne = -1;
            int rotaChangeLastAssignedStaffTwo = -1;

            var runningDate = startDate;
            for (int i = 0; i < totalWorkingDays; i++)
            {
                runningDate = runningDate.GetNextWorkingDate();
                var randomStaffMemberAM = GetRandomStaffMemberFromPool(random, staffPool, runningDate);
                var randomStaffMemberPM = GetRandomStaffMemberFromPool(random, staffPool, runningDate);

                ShiftView shift = new ShiftView()
                {
                    Date = runningDate,
                    ShiftStaffMemberAM = randomStaffMemberAM,
                    ShiftStaffMemberPM = randomStaffMemberPM
                };

                //Add the Rota change staff back into the pool after the rota has changed and we have assigned the first day
                if (rotaChangeLastAssignedStaffOne != -1)
                {
                    staffPool.Add(rotaChangeLastAssignedStaffOne, null);
                    rotaChangeLastAssignedStaffOne = -1;
                    staffPool.Add(rotaChangeLastAssignedStaffTwo, null);
                    rotaChangeLastAssignedStaffTwo = -1;
                }

                //Reset the staff pool as we have used up the remaining staff
                if (!staffPool.Any())
                {
                    staffPool = GetStaffShiftList();
                    shift.RotaReset = true; //This is mainly useful for debuggin purposes

                    //Remove last assigned staff from pool as they can't be assigned conescutively - They will be added in after first day assignment
                    rotaChangeLastAssignedStaffOne = randomStaffMemberAM.Id;
                    staffPool.Remove(randomStaffMemberAM.Id);

                    rotaChangeLastAssignedStaffTwo = randomStaffMemberPM.Id;
                    staffPool.Remove(randomStaffMemberPM.Id);
                }

                rota.Add(shift);
            }

            return rota;
        }

        /// <summary>
        /// Get a staff shift list containing the staff id and last assigned shift date
        /// </summary>
        /// <returns>The staff shift list</returns>
        public Dictionary<int, DateTime?> GetStaffShiftList()
        {
            var staffShiftList = Staff.ToDictionary(x => x.Key, x => (DateTime?)null);

            if (!staffShiftList.Any())
            {
                throw new InvalidOperationException("We don't have any staff so we can't generate a rota");
            }

            return staffShiftList;
        }

        /// <summary>
        /// Get a random staff member from a given staff pool
        /// </summary>
        /// <param name="random">The instance of Random to use for random selection</param>
        /// <param name="staffPool">The staff pool to select a staff member from</param>
        /// <param name="runningDate">The current running date</param>
        /// <returns>A random staff member from a given staff pool</returns>
        private StaffView GetRandomStaffMemberFromPool(Random random, Dictionary<int, DateTime?> staffPool, DateTime runningDate)
        {
            /*Get a random person for shift where they have not been assigned a shift yet
             * or were not assigned a shift on the last working day*/
            var availableStaffMembers = staffPool.Where(x => !x.Value.HasValue || x.Value.Value.Date < runningDate.GetPreviousWorkingDay().Date)
                .ToList();

            /*If we have less than 5 staff members, then we don't have enough left to randomise with
             * so we will return them in order of oldest first - null/assigned being first in the list*/
            int randomStaffMemberId = -1;
            if (availableStaffMembers.Count < 5)
            {
                randomStaffMemberId = availableStaffMembers.OrderBy(x => x.Value).FirstOrDefault().Key;
            }
            else
            {
                randomStaffMemberId = availableStaffMembers[random.Next(availableStaffMembers.Count)].Key;
            }

            var staffShiftControlItem = staffPool[randomStaffMemberId];
            if (staffShiftControlItem.HasValue)
            {
                //Staff member aready have two shifts so remove from staff pool
                staffPool.Remove(randomStaffMemberId);
            }
            else
            {
                staffPool[randomStaffMemberId] = runningDate; //Otherwise set the first shift date, changing it from null
            }

            return new StaffView()
            {
                Id = randomStaffMemberId,
                Name = Staff[randomStaffMemberId]
            };
        }

        private static List<string> MoveItemFromEndToBegining(List<string> list)
        {
            int lastItemIndex = list.Count - 1;

            var item = list[lastItemIndex];
            list.RemoveAt(lastItemIndex);
            list.Insert(0, item);
            return list;
        }
    }
}