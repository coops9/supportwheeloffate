﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WheelOfFateRota.Business
{
    public class StaffView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public static List<StaffView> GetActiveStaff()
        {
            using (var repository = new StaffRepository())
            {
                //Lets store a copy of the staff list 
                return repository.GetActiveStaff().Select(x =>
                new StaffView() {
                    Id = x.Id,
                    Name = x.Name,
                    IsActive = x.IsActive
                }).ToList();
            }
        }
    }
}