﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WheelOfFateRota.Business;

namespace WheelOfFateRota.Controllers
{
    public class RotaController : ApiController
    {
        // GET api/<controller>
        public object Get(int id = 0)
        {
            //This is a slightly contrived way of asking for a new rota. todo: Improve this method request
            bool newRota = id == -1;

            try
            {
                var shiftView = new ShiftView();
                var rota = shiftView.GetRota(newRota);

                //Lets code defensively and return an error code that helps the caller diagnose problems
                if (rota == null)
                {
                    return NotFound();
                }
                else
                {
                    return rota;
                }
            }
            catch (Exception ex)
            {
                //todo: We should log the exception
                //In case an Exception occurs we will return an appropriate return status code
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
                //return InternalServerError();
            }
        }
    }
}