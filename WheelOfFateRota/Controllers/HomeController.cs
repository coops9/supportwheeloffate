﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WheelOfFateRota.Business;

namespace WheelOfFateRota.Controllers
{
    public class HomeController : Controller
    {
        private const string ApiRotaUrl = "api/rota";
        private const string ApiStaffUrl = "api/staff";

        /*Note: We could use a seperate controller or Ajax POST for the clear but this does the job
         * and works quite well because we want the same behaviour anyway after the clear which is a new rota*/
        public ActionResult Index(bool clear = false)
        {
            using (var client = GetClient())
            {
                HttpResponseMessage response = client.GetAsync(ApiRotaUrl + (clear ? "/-1" : string.Empty)).Result;

                if (clear)
                {
                    return RedirectToAction("Index"); //todo: Improve - Removes the 'clear' query parameter
                }

                if (response.IsSuccessStatusCode)
                {
                    var yourcustomobjects = JsonConvert.DeserializeObject<IEnumerable<ShiftView>>
                        (response.Content.ReadAsStringAsync().Result);


                    return View(yourcustomobjects);
                }
                else
                {
                    //todo: loggging - instead for now we will just expose the error

                    //Attempt to get exception details 
                    string errorMessage = string.Empty;
                    try
                    {
                        var result = response.Content.ReadAsAsync<HttpError>().Result;
                        errorMessage = result.ExceptionMessage;
                    }
                    catch (Exception ex)
                    {
                        //todo: Also log - including stack 
                    }

                    //Return a message with some context, sometimes useful beyond just having a stack trace.
                    //This will actually hide the stck trace, but if we are logging, then we can log this
                    return Content($"Sorry. Something went wrong when trying to contact the API: {errorMessage}");
                }
            }
        }

        public ActionResult Staff()
        {
            using (var client = GetClient())
            {
                HttpResponseMessage response = client.GetAsync(ApiStaffUrl).Result;

                if (response.IsSuccessStatusCode)
                {
                    var yourcustomobjects = JsonConvert.DeserializeObject<IEnumerable<StaffView>>
                        (response.Content.ReadAsStringAsync().Result);


                    return View(yourcustomobjects);
                }
                else
                {
                    //todo: loggging - instead for now we will just expose the error

                    //Attempt to get exception details 
                    string errorMessage = string.Empty;
                    try
                    {
                        var result = response.Content.ReadAsAsync<HttpError>().Result;
                        errorMessage = result.ExceptionMessage;
                    }
                    catch (Exception ex)
                    {
                        //todo: Also log - including stack 
                    }

                    //Return a message with some context, sometimes useful beyond just having a stack trace.
                    //This will actually hide the stck trace, but if we are logging, then we can log this
                    return Content($"Sorry. Something went wrong when trying to contact the API: {errorMessage}");
                }
            }
        }

        private static HttpClient GetClient()
        {
            return new HttpClient
            {
                BaseAddress = new Uri("http://localhost:50888/")
            };
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}